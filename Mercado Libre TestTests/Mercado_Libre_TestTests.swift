@testable import Mercado_Libre_Test
import XCTest

class Mercado_Libre_TestTests: XCTestCase {
    var getProducts: GetProducts?
    var products: [Product] = []
    var error: Any?

    override func setUpWithError() throws {}

    override func tearDownWithError() throws {}

    func testGetAllProductsAndReturnsProducts() {
        givenARequestToGetProducts()
        whenTheActionOfGetAllProductsIsExecuted()
        thenMustReturnProducts()
    }

    func testGetAllproductsAndReturnsEmptyListOfProducts() {
        givenARequestToGetProducts()
        whenTheActionOfGetAllProductsIsExecutedWithEmptyQuery()
        thenMustReturnEmptyList()
    }

    func testGetAllproductsAndFail() {
        givenARequestToGetProducts()
        whenTheActionOfGetAllProductsIsExecutedAndReturnFail()
        thenMustReturErrorWithEmptyList()
    }
    
    func thenMustReturErrorWithEmptyList() {
        XCTAssertEqual(products.count, 0)
        XCTAssertEqual(error == nil, false)
    }
    
    
    func whenTheActionOfGetAllProductsIsExecutedAndReturnFail() {
        getProducts!.execute(query: "error") {
            products, error in
            self.products = products
            self.error = error
        }
    }

    func whenTheActionOfGetAllProductsIsExecutedWithEmptyQuery() {
        getProducts!.execute(query: "consulta_vacia") {
            products, error in
            self.products = products
            self.error = error
        }
    }

    func thenMustReturnEmptyList() {
        XCTAssertEqual(products.count, 0)
        XCTAssertEqual(error == nil, true)
    }

    func givenARequestToGetProducts() {
        getProducts = GetProducts(_apiClient: ApiClientWithResult())
    }

    func whenTheActionOfGetAllProductsIsExecuted() {
        getProducts!.execute(query: "prueba") {
            products, error in
            self.products = products
            self.error = error
        }
    }

    func thenMustReturnProducts() {
        XCTAssertEqual(products.count, 1)
        XCTAssertEqual(error == nil, true)
    }
}
