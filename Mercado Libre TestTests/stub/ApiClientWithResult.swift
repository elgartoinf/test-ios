import SwiftyJSON
import UIKit

class ApiClientWithResult: ApiClient {
    override func get(url _: String?, parameters: [String: Any]?, completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void) {
        if (parameters?["q"] as? String ?? "") == "prueba" {
            completionHandler(stringToJSON("{\"results\":[{\"title\":\"prueba\",\"price\":\"112\",\"currency_id\":\"COP\",\"soldQuantity\":\"11\",\"thumbnail\":\"sss\"}]}"), nil)
        }
        
        if (parameters?["q"] as? String ?? "") == "consulta_vacia" {
            completionHandler(stringToJSON("{}"), nil)
        }
        if (parameters?["q"] as? String ?? "") == "error" {
            completionHandler(stringToJSON("{}"), GenericError.GenericErrorApi)
        }
        
    }

    func stringToJSON(_ data: String) -> JSON {
        var json = JSON()
        do {
            json = try JSON(data: data.data(using: .utf8)!)
        } catch {}
        return json
    }
}

enum GenericError: Error {
    case GenericErrorApi
}
