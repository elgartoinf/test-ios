import UIKit

class ProductDetailViewController: UIViewController {
    @IBOutlet var thumbnail: UIImageView!
    @IBOutlet var price: UILabel!
    @IBOutlet var id: UILabel!
    @IBOutlet var productTitle: UILabel!

    var product: Product?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = .black

        thumbnail.load(url: product!.thumbnail)
        price.text = product!.getFullPrice()
        id.text = product!.id
        productTitle.text = product!.title
    }
}
