import UIKit

class ListOfProductsTableViewController: UITableViewController {
    var resultSearchController = UISearchController()
    var activityIndicator = UIActivityIndicatorView(style: .large)

    var products: [Product] = []

    func searchProduct(query: String) {
        GetProducts(_apiClient: ApiClient()).execute(query: query) {
            products, error in
            self.tableView.separatorStyle = .singleLine
            if error != nil {
                self.resultSearchController.isActive = false
                self.activityIndicator.stopAnimating()
                Alert.showAlert(topController: self, title: NSLocalizedString("Error de conexión", comment: ""), message: "\(String(describing: error))")
                return
            }

            self.products = products
            self.activityIndicator.stopAnimating()
            self.tableView.reloadData()

            if products.count == 0 {
                self.showViewMessage(message: NSLocalizedString("No hay resultados para tu búsqueda", comment: ""))
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.center = view.center
        view.addSubview(activityIndicator)

        resultSearchController = {
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.delegate = self
            controller.searchBar.sizeToFit()

            tableView.tableHeaderView = controller.searchBar

            return controller
        }()

        showViewMessage(message: NSLocalizedString("Usa el buscador para encontrar tus productos", comment: ""))
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! ProductCell
        let product = products[indexPath.row]
        cell.id.text = product.price
        cell.price.text = product.getFullPrice()
        cell.title.text = product.title
        cell.thumbnail.load(url: product.thumbnail)
        return cell
    }

    override func numberOfSections(in _: UITableView) -> Int {
        return 1
    }

    override func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return products.count
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let cell = sender as? UITableViewCell {
            let index = tableView.indexPath(for: cell)!.row
            if segue.identifier == "showProductDetail" {
                let vc = segue.destination as! ProductDetailViewController
                vc.product = products[index]
            }
        }
    }

    func showViewMessage(message: String) {
        let messageLabel = UILabel(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: view.bounds.size.width, height: view.bounds.size.height)))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.sizeToFit()
        tableView.backgroundView = messageLabel
        tableView.separatorStyle = .none
    }
}

extension ListOfProductsTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        activityIndicator.startAnimating()
        let searchText = searchController.searchBar.text?.lowercased() ?? ""
        if searchText == "" {
            activityIndicator.stopAnimating()
            return
        }
        searchProduct(query: searchText)
        tableView.reloadData()
    }
}

extension ListOfProductsTableViewController: UISearchControllerDelegate {
    func didDismissSearchController(_: UISearchController) {
        tableView.reloadData()
    }
}
