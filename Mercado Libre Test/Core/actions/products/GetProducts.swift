import Foundation

class GetProducts: NSObject {
    var apiClient: ApiClient

    init(_apiClient: ApiClient) {
        apiClient = _apiClient
    }

    func execute(query: String?, completionHandler: @escaping (_ products: [Product], _ error: Error?) -> Void) {
        apiClient.get(url: "search", parameters: ["q": query ?? ""]) { result, error in
            var products: [Product] = []

            if error != nil {
                completionHandler(products, error)
                return
            }

            for productDictionary in result["results"].arrayValue {
                let product = Product(
                    id: productDictionary["id"].stringValue,
                    title: productDictionary["title"].stringValue,
                    price: productDictionary["price"].stringValue,
                    currency: productDictionary["currency_id"].stringValue,
                    soldQuantity: productDictionary["soldQuantity"].intValue,
                    thumbnail: productDictionary["thumbnail"].stringValue
                )
                products.append(product)
            }

            completionHandler(products, error)
        }
    }
}
