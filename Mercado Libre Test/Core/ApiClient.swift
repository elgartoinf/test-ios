import Alamofire
import SwiftyJSON

class ApiClient: NSObject {
    var baseURL = "https://api.mercadolibre.com/sites/MCO/"
    func get(url: String?, parameters: [String: Any]?, completionHandler: @escaping (_ result: JSON, _ error: Error?) -> Void) {
        baseURL.append(url ?? "")

        Alamofire.request(baseURL, method: .get, parameters: parameters).validate().responseJSON { response in
            switch response.result {
            case let .success(value):
                completionHandler(JSON(value), nil)
            case let .failure(error):
                completionHandler(JSON(), error)
            }
        }
    }
}
