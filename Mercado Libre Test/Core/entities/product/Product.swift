import Foundation

class Product: NSObject {
    var id: String
    var title: String
    var price: String
    var currency: String
    var soldQuantity: Int
    var thumbnail: String

    init(id: String, title: String, price: String, currency: String, soldQuantity: Int, thumbnail: String) {
        self.id = id
        self.title = title
        self.price = price
        self.currency = currency
        self.soldQuantity = soldQuantity
        self.thumbnail = thumbnail
    }

    func getFullPrice() -> String {
        return "\(price) \(currency)"
    }
}
