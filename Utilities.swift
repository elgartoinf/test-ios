import UIKit

extension UIImageView {
    func load(url: String) {
        // start animation
        let activityIndicator = UIActivityIndicatorView(style: .large)
        addSubview(activityIndicator)
        activityIndicator.startAnimating()

        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: URL(string: url)!) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        // stop animation
                        activityIndicator.stopAnimating()
                    }
                }
            }
        }
    }
}

final class Alert: NSObject {
    static func showAlert(topController: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(
            title: NSLocalizedString("Aceptar", comment: ""),
            style: .cancel,
            handler: nil
        ))

        topController.present(alert, animated: true)
    }
}
